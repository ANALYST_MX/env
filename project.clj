(defproject env "0.1.0-SNAPSHOT"
  :description "Environment Variables"
  :url "https://bitbucket.org/ANALYST_MX/env"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]])

# env

Mapping environment variables can be a bit of a pain.

## Usage


```
#!BASH

$ export ENV_TEST1="TEST1"
$ export ENV_TEST2="TEST2"
$ export ENV_TEST3="TEST3"
$ export ENV_TEST4="TEST4"
```



```
#!clojure

(prefix "ENV_") #=> {"test4" "TEST4", "test2" "TEST2", "test1" "TEST1", "test3" "TEST3"}
```


## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
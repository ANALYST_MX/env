(ns env.core-test
  (:require [clojure.test :refer :all]
            [env.core :refer :all]))

(println (str "Using Clojure version " *clojure-version*))

(deftest test-urlparse
  (is (= {"site:" {:path "/ANALYST_MX/env", :user-info nil, :fragment nil, :authority "bitbucket.org", :port -1, :host "bitbucket.org", :url "https://bitbucket.org/ANALYST_MX/env", :query nil, :scheme "https"}}
         (urlparse {"site:" "https://bitbucket.org/ANALYST_MX/env"}))))

(deftest test-map-env
  (is (= {"test1" "test1", "test3" "test3", "test2" "test2", "test4" "test4"}
         (map-env {"TEST1" "TEST1", "TEST2" "TEST2", "TEST3" "TEST3", "TEST4" "TEST4"}))))

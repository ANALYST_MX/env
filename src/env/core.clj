(ns env.core
  (:use [clojure.string :only [lower-case]])
  (:import [java.net URI]))

(def ^:const SLASH "/")
(def ^:const BLANK-STRING "")

(defn- uri-or-nil
  [^String s]
  (try
    (URI. s)
    (catch java.net.URISyntaxException e nil)))

(defn- map-function-on-map-keys [m f]
  (into {} (for [[k v] m] [(f k) v])))

(defn- starts-with?
  [^String s ^String prefix]
  (.startsWith s prefix))

(defn- filter-map [m f]
  (select-keys m (for [[k v] m :when (f k)] k)))

(defprotocol ParseResult
  (^String url       [input])
  (^String scheme    [input])
  (^String host      [input])
  (^String authority [input])
  (^String port      [input])
  (^String user-info [input])
  (^String path-url  [input])
  (^String query     [input])
  (^String fragment  [input]))

(extend-protocol ParseResult
  URI
  (url [^URI input]
    (.toString input))
  (scheme [^URI input]
    (when-let [s (.getScheme input)]
      (.toLowerCase s)))
  (host [^URI input]
    (-> input .getHost .toLowerCase))
  (authority [^URI input]
    (-> input .getAuthority .toLowerCase))
  (port [^URI input]
    (.getPort input))
  (user-info [^URI input]
    (.getUserInfo input))
  (path-url [^URI input]
    (let [path (.getPath input)]
      (if (or (nil? path) (= SLASH path) (= BLANK-STRING path))
        SLASH
        path)))
  (query [^URI input]
    (.getQuery input))
  (fragment [^URI input]
    (.getFragment input))

  nil
  (url       [input]
    nil)
  (scheme    [input]
    nil)
  (host      [input]
    nil)
  (authority [input]
    nil)
  (port      [input]
    nil)
  (user-info [input]
    nil)
  (path-url  [input]
    nil)
  (query     [input]
    nil)
  (fragment  [input]
    nil))

(defn encode-path [^String path]
  (let [uri (uri-or-nil path)]
    {:url (url uri)
     :scheme (scheme uri)
     :host (host uri)
     :authority (authority uri)
     :port (port uri)
     :user-info (user-info uri)
     :path (path-url uri)
     :query (query uri)
     :fragment (fragment uri)}))

(defn lower-dict
  [m]
  (map-function-on-map-keys m lower-case))

(defn urlparse
  ([m] (urlparse m (keys m)))
  ([m v]
   (merge m (into {} (for [key v] [key (encode-path (m key))])))))

(defn prefix
  [prefix]
  (let [env (lower-dict (System/getenv))
        prefix (lower-case prefix)
        len-prefix (count prefix)]
    (-> env
        (filter-map #(starts-with? % prefix))
        (map-function-on-map-keys #(subs % len-prefix)))))

(defn map-env
  [kwargs]
  (let [env (lower-dict kwargs)]
    (into {} (for [[k v] env] [k (lower-case v)]))))
